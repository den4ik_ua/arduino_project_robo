#include <Arduino.h>
#include <DHT.h>
#include <MotorL298.h>

#define DHTPIN 2 // номер пина, к которому подсоединен датчик

#define BLE_STATE_PIN 4 // pin for get state BLE

#define MOTOR_LEFT 13
#define MOTOR_LEFT_REVERS 12
#define MOTOR_LEFT_SPEED 10

#define MOTOR_RIGHT 11
#define MOTOR_RIGHT_REVERS 8
#define MOTOR_RIGHT_SPEED 9

#define FORWARD 0
#define REVERS 1

const int COMMAND_GO_L = 1;
const int COMMAND_GO_R = 2;
const int COMMAND_REVERS_L = 3;
const int COMMAND_REVERS_R = 4;
const int COMMAND_STOP = 6;
const int COMMAND_SPEED = 7;

// Инициируем датчик
DHT dht(DHTPIN, DHT11);

//Инициализируем моторы
MotorL298 motorL(MOTOR_LEFT, MOTOR_LEFT_REVERS, MOTOR_LEFT_SPEED);
MotorL298 motorR(MOTOR_RIGHT, MOTOR_RIGHT_REVERS, MOTOR_RIGHT_SPEED);

int speedMotor = 255;

void setup() {
  Serial.begin(9600);
  dht.begin();

  pinMode(BLE_STATE_PIN, INPUT);

  motorL.setSpeed(0);
  motorR.setSpeed(0);
}

void loop() {
  readSensorsAndSend();
  receiveFromAndroid();

  delay(1);
}

void readSensorsAndSend() {
  //Считываем влажность
  int h = dht.readHumidity();
  // Считываем температуру
  int t = dht.readTemperature();
  // Проверка удачно прошло ли считывание.
  if (isnan(h) || isnan(t)) {
    Serial.println("Не удается считать показания");
    return;
  }
  //Output data of read from detector
  Serial.print("#");Serial.print(h);Serial.print("|");Serial.print(t);Serial.print("~");Serial.print("\n");
  delay(10);
  //#####################################
}

void receiveFromAndroid() {
  if(Serial.available() > 0){
    delay(1);
    int command = Serial.read();

    fetchSpeed(command);

    switch (command) {
      case COMMAND_GO_L:
          motorL.setSpeedPWM(speedMotor);
          motorL.setDirectionPWM(FORWARD);
      break;
      case COMMAND_REVERS_L:
          motorL.setSpeedPWM(speedMotor);
          motorL.setDirectionPWM(REVERS);
      break;
      case COMMAND_GO_R:
          motorR.setSpeedPWM(speedMotor);
          motorR.setDirectionPWM(REVERS);
      break;
      case COMMAND_REVERS_R:
          motorR.setSpeedPWM(speedMotor);
          motorR.setDirectionPWM(FORWARD);
      break;
      case COMMAND_STOP:
          motorL.setSpeedPWM(0);
          motorR.setSpeedPWM(0);
      break;
    }
  }
}

void fetchSpeed(int command) {
  int speed = speedMotor;

  if(command == COMMAND_SPEED){
    speed = Serial.read();
    speedMotor = speed;
  }
}
