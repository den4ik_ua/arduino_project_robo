/**
* DC motor library
*
* Copyright 2013 Vladimir Laykov 
*
* DUAL FULL-BRIDGE DRIVER L298
*
*
*  pinSpeed - pin PWM function
*/

#include "MotorL298.h"
#include "Arduino.h"

MotorL298::MotorL298(int pinInput1, int pinInput2, int pinSpeed)
{
  _pinIN1 = pinInput1;
  _pinIN2 = pinInput2;
  _pinSpeed = pinSpeed;
  // set pin mode
  pinMode( _pinIN1, OUTPUT );
  pinMode( _pinIN2, OUTPUT );
  pinMode( _pinSpeed, OUTPUT );
  // set speed 0
  setSpeed(0);
  // set motor stop
  setDirection(0);
  _currentDelay =  200; // 200 microseconds
  
}
/* ================ Public methods ================ */

void MotorL298::setSpeed(int speed)
{
 analogWrite(_pinSpeed,speed);
 _currentSpeed = speed;
}

void MotorL298::setDirection(int dir)
{
 if (dir == 0 ) 
 {
   digitalWrite(_pinIN1,HIGH);
   digitalWrite(_pinIN2,LOW);
   _currentDir = 0;

 }
 else
 {
   digitalWrite(_pinIN1,LOW);
   digitalWrite(_pinIN2,HIGH);
   _currentDir = 1;
 }

}

void MotorL298::setDirectionPWM(int dir)
{
  if (_currentDir != dir) 
  {
   int  _speed = _currentSpeed;
   setSpeedPWM(0);
   setDirection(dir);
   setSpeedPWM(_speed);
  }
}

void MotorL298::stop()
{
 digitalWrite(_pinIN1,LOW);
 digitalWrite(_pinIN2,LOW);
 _currentDir = 0;
}

void MotorL298::setSpeedPWM(int speed)
{
 unsigned long time;
 if (speed - _currentSpeed <= 0) 
  {  //замедляемся    _curr > speed
  for (int i=_currentSpeed; i >= speed; i--)
    {
    analogWrite(_pinSpeed,i);
    time = micros();
    while(micros()-time < _currentDelay){ }   // for more smoothly
    } 
  }
 else
  {  // ускоряемся    _curr < speed
  for (int i=_currentSpeed; i <= speed; i++)
    {
    analogWrite(_pinSpeed,i);
    time = micros();
    while(micros()-time < _currentDelay){}  // for more smoothly
    } 
  }
  _currentSpeed = speed;
}

void MotorL298::setDelay(int delaymcs)
{
 _currentDelay = delaymcs;
}