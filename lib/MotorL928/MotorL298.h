/**
* DC motor library
*
* Copyright 2013 Vladimir Laykov 
*
* DUAL FULL-BRIDGE DRIVER L298
*
*/

#ifndef MotorL298_h
#define MotorL298_h

#include <Arduino.h>

class MotorL298
{
  public:
    MotorL298(int pinInput1, int pinInput2, int pinSpeed);      // Constructor
    void setSpeed(int speed);                                   // set motor speed
    void setSpeedPWM(int speed);                                // sert motor speed PWM
    void setDirection(int dir);                                 // set direction motor rotation
    void setDirectionPWM(int dir);                              // set direction motor rotation  PWM
    void setDelay(int delaymcs);
    void stop();                                                // stop motor
  private:
    int _pinIN1;    // Digital pin1 for DC motor direction
    int _pinIN2;    // Digital pin2 for DC motor direction
    int _pinSpeed;  // PWM pin for speed control
    int _currentSpeed; // Curret DC speed
    int _currentDir;
    int _currentDelay;  // delay microsecond
};

#endif